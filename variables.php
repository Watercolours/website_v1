<?php
error_reporting(0);


$ROOT_DIR = $_SERVER["DOCUMENT_ROOT"];
$IMG_DIR = $_SERVER["SERVER_NAME"].'/img';

$images = glob("img/*.*");

$max = glob("img/3d/*");
$web = glob("img/web/*");
$ps = glob("img/ps/*");
$photo = glob("img/photo/*");

$full_name = $_SERVER['PHP_SELF'];
$name_array = explode('/', $full_name);
$count = count($name_array);
$page_name = $name_array[$count - 1];
$page_title = substr($page_name, 0, -4);

$menu_items = array(
    'portfolio' => 'portfolio.php',
    'about' => 'about.php'
);


$image_desc = array(
    'Diamonds.jpg' => '',
    'Dissolving Skull.jpg' => '',
    'Glass Apple.jpg' => '',
    'Pixel Figures.JPG' => '',
    'Red Eye.JPG' => '',
    'Shotgun.jpg' => '',
    'Splash Of Light.JPG' => '',
    'TIME IT.JPG' => '',
    'Watercolours Logo.PNG' => '',
    'Watercolours Poster.JPG' => '',
    'Zombie.JPG' => '',
    'album_mockup.JPG' => '',
    'apocalypse.jpg' => '',
    'blue_eye.JPG' => '',
    'butterfly.JPG' => '',
    'dream_room.JPG' => '',
    'eye_in_the_storm.JPG' => '',
    'grim_reaper - clay_render.jpg' => '',
    'lamp.JPG' => '',
    'oslo_opera_mockup.jpg' => '',
);


//=============================
//======== ABOUT PAGE =========
//=============================
$broadnet = array(
    'Broadnet AS',
    'Sep 2013 - Present',
    'System / Front-end Developer'
);

$octaga = array(
    'Octaga VS',
    'Jan 2013 - May 2013',
    'Developer, Designer & 3D-Artist (Bachelor assignment)'
);

$experience = array(
    $broadnet,
    $octaga
);

$nith = array(
    'Norwegian School of Internet Technology (NITH)',
    'Aug 2010 - June 2013',
    'Bachelor of Science in Information Technology'
);

$rvgs = array(
    'Ringerike Upper Secondary School',
    'Aug 2007 - June 2010',
    'Music'
);

$education = array(
    $nith,
    $rvgs
);