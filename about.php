<?php
require_once('header.php');
require_once('variables.php');
?>

<div id="about">

    <h2>
        Skills
    </h2>
    <h3>
        Code
    </h3>
    <p>
        Java - JavaScript - jQuery - CSS / LESS - HTML - PostgreSQL - Wicket - Hibernate - JSON - XML - REST
    </p>
    <h3>
        Design
    </h3>
    <p>
        Photoshop - 3Ds Max
    </p>
    <br/>

<?php
if (is_array($experience)) {
    echo("<h2>Experience</h2>");
    foreach ($experience as $entry) {
        extractArray($entry);
    }
}
echo("<br/>");
if (is_array($education)) {
    echo("<h2>Education</h2>");
    foreach ($education as $entry) {
        extractArray($entry);
    }
}
echo "</div>";

require_once('footer.php');

function extractArray($entries)
{
    if (is_array($entries)) {
        echo "<div>";
        foreach ($entries as $entry) {
            if ($entry == $entries[0]) {
                echo "<h3>" . $entry . "</h3>";
            } else {
                echo "<p>" . $entry . "</p>";
            }
        }
        echo "</div>";
    }
}
