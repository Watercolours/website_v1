<?php require_once('variables.php'); ?>

<html>
<head>
    <link href='http://fonts.googleapis.com/css?family=Raleway:200,300,100' rel='stylesheet' type='text/css'>
    <link type="text/css" rel="stylesheet/less" href="/less/style.less">
    <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="js/less-1.5.0.min.js"></script>

    <title>
        Nicolas Nordhagen
    </title>
</head>

<body>
<div id="index">
    <div id="center">
        <div id="logo">
            <h1>NICOLAS NORDHAGEN</h1>
        </div>

        <ul>
            <?php foreach ($menu_items as $title => $href): ?>
                <li>
                    <a href="<?php echo($href); ?>">
                        <?php echo(($page_title == $href) ? '+' : ''); ?>
                        <?php echo($title); ?>
                    </a>
                </li>
            <?php endforeach ?>
        </ul>

        <img src="img/ps/Watercolours%20Logo.PNG">
    </div>
</div>
</body>
</html>