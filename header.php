<?php require_once("variables.php"); ?>

<html>
<head>
    <link href='http://fonts.googleapis.com/css?family=Raleway:200,300,100' rel='stylesheet' type='text/css'>
    <link type="text/css" rel="stylesheet/less" href="/less/style.less">
    <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="js/less-1.5.0.min.js"></script>
    <script type="text/javascript" src="js/nn.js"></script>

    <script type="text/javascript" src="js/lightbox2.js"></script>
    <link type="text/css" rel="stylesheet" href="/less/lightbox2.css">

    <title>
        Nicolas Nordhagen
    </title>
</head>

<body>

<div id="container">

<header>
    <div id="logo">
        <h1>NICOLAS NORDHAGEN</h1>
    </div>

    <nav id="menu">
        <ul>
            <?php foreach ($menu_items as $title => $href): ?>
                <li>
                    <a href="<?php echo($href); ?>" class="<?php echo(($page_title == $title) ? 'active' : ''); ?>">
                        <?php echo(($page_title == $title) ? ('+'.$title) : $title); ?>
                    </a>
                </li>
            <?php endforeach ?>
        </ul>
    </nav>
</header>

